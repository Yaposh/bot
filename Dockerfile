FROM golang:1.14.4

COPY . go/src
WORKDIR go/src

ARG INTEGRATIONS_VERSION
ENV INTEGRATIONS_VERSION=$INTEGRATIONS_VERSION


ARG INTEGRATIONS_COMMIT
ENV INTEGRATIONS_COMMIT=$INTEGRATIONS_COMMIT

RUN if [ -n "$INTEGRATIONS_VERSION" ]; then  INTEGRATIONS_VERSION=Nice_master; make build; \
    else INTEGRATIONS_VERSION=lomaster; INTEGRATIONS_COMMIT=mycommit; echo Argument is $INTEGRATIONS_VERSION ; \
    INTEGRATIONS_COMMIT=$(node -p "require('./commit.json').title"); echo Commit is $INTEGRATIONS_COMMIT; \
    make build; \
    fi





