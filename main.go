package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bot/pkg/bot"
)

func main() {

	ver := os.Getenv("INTEGRATIONS_VERSION")
	log.Printf("Version is %q", ver)

	commit := os.Getenv("INTEGRATIONS_COMMIT")
	log.Printf("Commit is %q", commit)

	bot := bot.New()
	http.HandleFunc("/about", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "About: ", bot.SendMsg("urich bot"))
	})

	fmt.Println("Server is listening...")
	if err := http.ListenAndServe("localhost:8080", nil); err != nil {
		panic(err)
	}
}
